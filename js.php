    <!-- JavaScript files-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/messenger/js/messenger.min.js"></script>
    <script src="vendor/messenger/js/messenger-theme-flat.js"></script>
    <script src="vendor/jquery-validation/jquery.validate.min.js"></script>

    <!-- Data Tables-->
    <!-- <script src="https://d19m59y37dris4.cloudfront.net/admin-premium/1-4-3/vendor/datatables.net/js/jquery.dataTables.js"></script>
    <script src="https://d19m59y37dris4.cloudfront.net/admin-premium/1-4-3/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
    <script src="https://d19m59y37dris4.cloudfront.net/admin-premium/1-4-3/vendor/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="https://d19m59y37dris4.cloudfront.net/admin-premium/1-4-3/vendor/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>
    <script src="https://d19m59y37dris4.cloudfront.net/admin-premium/1-4-3/js/tables-datatable.js"></script> -->
    
    <script src="vendor/datatables-1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-1.10.16/js/dataTables.bootstrap4.min.js"></script>

    <!-- Main File-->
    <script src="js/front.js"></script>
    <script src="js/custom.js"></script>