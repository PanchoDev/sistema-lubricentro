<?php
include_once "class.conex.php";

class Usuario{

	private $db;
	private $salt;
	private $table;

	function Usuario(){
		$this->db = new conexion();
		$this->salt = "7p69tiDcjRKhYJlN1Wf48";	
		$this->table = "usuario";
	}
	
	function getListaUsuarios($arr)
	{
		//$offset = ($page-1)*$reg;
		
		$where = "";
		$where .= isset($arr['ft_nombre']) && $arr['ft_nombre'] != "" ? " AND usu_nombre LIKE '%".$this->db->db_real_escape_string($arr['ft_nombre'])."%'" : "";
		$where .= isset($arr['ft_email']) && $arr['ft_email'] != "" ? " AND usu_email LIKE '%".$this->db->db_real_escape_string($arr['ft_email'])."%'" : "";
		$where .= isset($arr['ft_estado']) && $arr['ft_estado'] != 2 ? " AND usu_activo = ".$arr['ft_estado']."" : "";
		$where .= isset($arr['ft_fecha_ing_in']) && $arr['ft_fecha_ing_in'] != "" ? " AND DATE_FORMAT(usu_fecha_ing,'%Y-%m-%d') >= '".date("Y-m-d",strtotime($arr['ft_fecha_ing_in']))."'" : "";
		$where .= isset($arr['ft_fecha_ing_fi']) && $arr['ft_fecha_ing_fi'] != "" ? " AND DATE_FORMAT(usu_fecha_ing,'%Y-%m-%d') <= '".date("Y-m-d",strtotime($arr['ft_fecha_ing_fi']))."'" : "";
		
		$qry = "SELECT
					usu_id AS 'id',
					usu_rut AS 'rut',
					usu_nombre AS 'nombre',
					usu_apellido AS 'apellido',
					usu_usuario AS 'usuario',
					usu_activo AS 'activo',
					usu_fecha_creacion AS 'fecha_creacion'
				FROM usuario WHERE 1 ".$where;

		$res = $this->db->db_query($qry);
		$rows = array();
		//$result['total'] = $this->db->db_numrows($this->db->db_query($qry));
		while($row = $this->db->db_fetch_object($res))
		{
			array_push($rows, $row);
		}		

		$result['data'] = $rows;
		echo json_encode($result);
	}
	
	
	
	function buscarNombreSucursal($id)
	{
		$qry = "SELECT ususuc_usu_id FROM sgcp_usuario_sucursal WHERE ususuc_suc_id = ".$id."";
		$res = $this->db->db_query($qry);
		$rows = array();
		while($row = $this->db->db_fetch_array($res))
		{
			array_push($rows , $row['ususuc_usu_id']);
		}
		return $rows;
	}
	function validarExisteEmail($email,$idUsuario = 0)
	{
		$qry = "SELECT * FROM ".$this->table." WHERE usu_email = '".$email."' AND usu_email != '' AND usu_id NOT IN(".$idUsuario.") GROUP BY usu_email ";	
		$res = $this->db->db_query($qry);
		$total = $this->db->db_numrows($res);
		if($total > 0)
		{
			return true;	
		}
		else
		{
			return false;	
		}
	}
	function encriptPass($pass)
	{
		return  base64_encode(hash_hmac('md5', $pass, $this->salt));
	}

	function get_datos_usuario($arr){
		$qry = "SELECT usu_nombre,usu_email FROM ".$this->table." WHERE usu_id = ".$arr['idUsuario']."";
		$res = $this->db->db_query($qry);
		$row = $this->db->db_fetch_object($res);
		echo "var datos = ".json_encode($row);
	}
	function getNombreUsuario($id){
		$qry = "SELECT usu_nombre FROM ".$this->table." WHERE usu_id = ".$id."";
		$res = $this->db->db_query($qry);
		$row = $this->db->db_fetch_object($res);
		
		return $row->usu_nombre;
	}
	function getPerfilUsuario($id){
		$qry = "SELECT usu_perfil FROM ".$this->table." WHERE usu_id = ".$id."";
		$res = $this->db->db_query($qry);
		$row = $this->db->db_fetch_object($res);
		
		return $row->usu_perfil;
	}
	
	function cambio_clave_usuario($arr){
		$sql = "SELECT * FROM ".$this->table." WHERE usu_password = '".$this->encriptPass($arr['actual'])."' AND usu_id = ".$arr['idUsuario']."";
		$res = $this->db->db_query($sql);
		$total = $this->db->db_numrows($res);
		if($total <= 0){
			echo " var result = 'NOCOINCIDE' ;";
		}else{
			$qry = "UPDATE ".$this->table." SET usu_password = '".$this->encriptPass($arr['nuevaPass'])."' WHERE usu_id = ".$arr['idUsuario']."";
			if($this->db->db_query($qry))
			{
				echo " var result = 'OK' ;";
			}
			else
			{
				echo " var result = 'FAIL' ;";
			}
		}
	}
	
}


?>