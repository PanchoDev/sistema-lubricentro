$(document).ready(function () {

    'use strict';

    // ------------------------------------------------------- //
    // Search Box
    // ------------------------------------------------------ //
    $('#search').on('click', function (e) {
        e.preventDefault();
        $('.search-box').fadeIn();
    });
    $('.dismiss').on('click', function () {
        $('.search-box').fadeOut();
    });

    // ------------------------------------------------------- //
    // Card Close
    // ------------------------------------------------------ //
    $('.card-close a.remove').on('click', function (e) {
        e.preventDefault();
        $(this).parents('.card').fadeOut();
    });


    // ------------------------------------------------------- //
    // Adding fade effect to dropdowns
    // ------------------------------------------------------ //
    $('.dropdown').on('show.bs.dropdown', function () {
        $(this).find('.dropdown-menu').first().stop(true, true).fadeIn();
    });
    $('.dropdown').on('hide.bs.dropdown', function () {
        $(this).find('.dropdown-menu').first().stop(true, true).fadeOut();
    });


    // ------------------------------------------------------- //
    // Sidebar Functionality
    // ------------------------------------------------------ //
    $('#toggle-btn').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');

        $('.side-navbar').toggleClass('shrinked');
        $('.content-inner').toggleClass('active');
        $(document).trigger('sidebarChanged');

        if ($(window).outerWidth() > 1183) {
            if ($('#toggle-btn').hasClass('active')) {
                $('.navbar-header .brand-small').hide();
                $('.navbar-header .brand-big').show();
            } else {
                $('.navbar-header .brand-small').show();
                $('.navbar-header .brand-big').hide();
            }
        }

        if ($(window).outerWidth() < 1183) {
            $('.navbar-header .brand-small').show();
        }
    });

    // ------------------------------------------------------- //
    // Universal Form Validation
    // ------------------------------------------------------ //

    $('.form-validate').each(function() {  
        $(this).validate({
            errorElement: "div",
            errorClass: 'is-invalid',
            validClass: 'is-valid',
            ignore: ':hidden:not(.summernote, .checkbox-template, .form-control-custom),.note-editable.card-block',
            errorPlacement: function (error, element) {
                // Add the `invalid-feedback` class to the error element
                error.addClass("invalid-feedback");
                console.log(element);
                if (element.prop("type") === "checkbox") {
                    error.insertAfter(element.siblings("label"));
                } 
                else {
                    error.insertAfter(element);
                }
            }
        });

    });    

    // ------------------------------------------------------- //
    // Material Inputs
    // ------------------------------------------------------ //

    var materialInputs = $('input.input-material');

    // activate labels for prefilled values
    materialInputs.filter(function() { return $(this).val() !== ""; }).siblings('.label-material').addClass('active');

    // move label on focus
    materialInputs.on('focus', function () {
        $(this).siblings('.label-material').addClass('active');
    });

    // remove/keep label on blur
    materialInputs.on('blur', function () {
        $(this).siblings('.label-material').removeClass('active');

        if ($(this).val() !== '') {
            $(this).siblings('.label-material').addClass('active');
        } else {
            $(this).siblings('.label-material').removeClass('active');
        }
    });

    // ------------------------------------------------------- //
    // Footer 
    // ------------------------------------------------------ //   

    var contentInner = $('.content-inner');

    $(document).on('sidebarChanged', function () {
        adjustFooter();
    });

    $(window).on('resize', function () {
        adjustFooter();
    })

    function adjustFooter() {
        var footerBlockHeight = $('.main-footer').outerHeight();
        contentInner.css('padding-bottom', footerBlockHeight + 'px');
    }

    // ------------------------------------------------------- //
    // External links to new window
    // ------------------------------------------------------ //
    $('.external').on('click', function (e) {

        e.preventDefault();
        window.open($(this).attr("href"));
    });

    // ------------------------------------------------------ //
    // For demo purposes, can be deleted
    // ------------------------------------------------------ //

    var stylesheet = $('link#theme-stylesheet');
    $("<link id='new-stylesheet' rel='stylesheet'>").insertAfter(stylesheet);
    var alternateColour = $('link#new-stylesheet');

    if ($.cookie("theme_csspath")) {
        alternateColour.attr("href", $.cookie("theme_csspath"));
    }

    $("#colour").change(function () {

        if ($(this).val() !== '') {

            var theme_csspath = 'css/style.' + $(this).val() + '.css';

            alternateColour.attr("href", theme_csspath);

            $.cookie("theme_csspath", theme_csspath, {
                expires: 365,
                path: document.URL.substr(0, document.URL.lastIndexOf('/'))
            });

        }

        return false;
    });


	$("#formLogin").validate({
		 rules: {
			loginUsername: {
				required: true
			},
			loginPassword: {
				required: true
			}
		},
		messages: {
			loginUsername: {
				required: "Debe ingresar su usuario"
			},
			loginPassword: {
				required: "Debe ingresar su contraseña",
			}
		},
		errorElement: 'span',
		errorClass: 'is-invalid invalid-feedback',		
		highlight: function(element) {
		   $(element).addClass('has_error is-invalid');
		},
		submitHandler: function(form) {
			// do other things for a valid form
			
			var usuario = $("#loginUsername").val();
			var password = $("#loginPassword").val();
			
			$.ajax({
				url: "login_ajax.php",
				type: "POST",
				data: {	
					acc: 1,
					txtEmail: usuario,
					txtPasswd: password
				},
				error: function(e){
					alert("error " + e.responseText);
				},
				beforeSend: function(){
					//$("#btnGuardar").prop("disabled", true);
				},
				success: function(data){
					$.globalEval(data);
					if(r.resultado == "OK"){
						Messenger().post({message: r.mensaje, type:"success"});
						location.href = r.irURL;
					}else{
						Messenger().post({message: r.mensaje, type:"danger"});
					}
					
				}
			});
		}
	});
	
	$("#formMensaje").validate({
		 rules: {
			txtTitulo: {
				required: true,
				maxlength: 100
			},
			txtSubTitulo: {
				required: true,
				maxlength: 100
			},
			txtURL: {
				url: true,
				maxlength: 100
			},
			txtMensaje: {
				required: true,
				maxlength: 150
			}
		},
		messages: {
			txtTitulo: {
				required: "Debe ingresar el titulo",
				maxlength: "El Titulo no debe exceder de 100 carácteres"
			},
			txtSubTitulo: {
				required: "Debe ingresar el subtitulo",
				maxlength: "El Subtitulo no debe exceder de 100 carácteres"
			},
			txtURL: {
				url: "Debe ingresar una URL",
				maxlength: "La URL no debe exceder de 100 carácteres"
			},
			txtMensaje: {
				required: "Debe ingresar su mensaje",
				maxlength: "El Mensaje no debe exceder de 150 carácteres"
			}
		},
		errorElement: 'span',
		errorClass: 'is-invalid invalid-feedback',		
		highlight: function(element) {
		   $(element).addClass('has_error is-invalid');
		},
			
		submitHandler: function(form) {
			// do other things for a valid form
			console.info("Form: " + $(form).serialize());
			
			var txtTitulo = $("#txtTitulo").val();
			var txtSubTitulo = $("#txtSubTitulo").val();
			var txtURL = $("#txtURL").val();
			var txtMensaje = $("#txtMensaje").val();
			
			$.ajax({
				url: "login_ajax.php",
				type: "POST",
				data: {	
					acc: 2,
					txtTitulo: txtTitulo,
					txtSubTitulo: txtSubTitulo,
					txtURL: txtURL,
					txtMensaje: txtMensaje
				},
				error: function(e){
					alert("error " + e.responseText);
				},
				beforeSend: function(){
					//$("#btnGuardar").prop("disabled", true);
				},
				success: function(data){
					$.globalEval(data);
					if(r.resultado == "OK"){
						
						Messenger().post({message: r.mensaje, type:"success"});
						//mostrar mensaje
						//Recargar el listado
					}else{
						Messenger().post({message: r.mensaje, type:"danger"});
					}
				}
			});
		}
	});
	
});




